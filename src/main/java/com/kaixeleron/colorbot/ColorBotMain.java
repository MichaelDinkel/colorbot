package com.kaixeleron.colorbot;

import com.kaixeleron.colorbot.config.ConfigManager;
import com.kaixeleron.colorbot.bot.BotThread;

import java.io.Console;

public class ColorBotMain {

    public static void main(String[] args) {

        ConfigManager configManager = new ConfigManager();

        if (configManager.isKeyMissing()) {

            System.out.println("Please specify the bot account's key in ColorBotConfig/config.txt");

        } else if (configManager.isChannelMissing()) {

            System.out.println("Please specify the announcement channel in ColorBotConfig/config.txt");

        } else {

            final BotThread thread = new BotThread(configManager.getKey(), configManager.getChannel());
            thread.start();

            try {

                //noinspection SynchronizationOnLocalVariableOrMethodParameter
                synchronized (thread) {

                    thread.wait();

                }

            } catch (InterruptedException ignored) {}

            if (thread.isConnectionSuccessful()) {

                System.out.println("Bot startup complete. Enter \"exit\" to exit.");

                Console console = System.console();
                String command;

                do {

                    command = console.readLine();

                    if (!command.equalsIgnoreCase("exit")) {

                        System.out.println("Unknown command. Enter \"exit\" to exit.");

                    }

                } while (!command.equalsIgnoreCase("exit"));

                System.out.println("Stopping bot...");
                thread.disconnect();

            }

        }

    }

}
