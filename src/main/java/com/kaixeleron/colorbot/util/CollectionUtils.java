package com.kaixeleron.colorbot.util;

import java.util.Collection;

public class CollectionUtils {

    private CollectionUtils() {}

    public static boolean containsIgnoreCase(Collection<String> collection, String value) {

        String[] values = collection.toArray(new String[0]);

        boolean found = false;
        int i = 0;

        while (!found && i < values.length) {

            if (values[i].equalsIgnoreCase(value)) {

                found = true;

            }

            i++;

        }

        return found;

    }

}
