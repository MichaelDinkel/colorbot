package com.kaixeleron.colorbot.bot;

import com.kaixeleron.colorbot.config.MessageConfigManager;
import com.kaixeleron.colorbot.config.RankConfigManager;
import com.kaixeleron.colorbot.util.CollectionUtils;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class ReactionListener extends ListenerAdapter {

    private final MessageConfigManager messageConfigManager;
    private final RankConfigManager rankConfigManager;

    public ReactionListener(MessageConfigManager messageConfigManager, RankConfigManager rankConfigManager) {

        this.messageConfigManager = messageConfigManager;
        this.rankConfigManager = rankConfigManager;

    }

    @Override
    public void onGuildMessageReactionAdd(@Nonnull GuildMessageReactionAddEvent event) {

        if (event.getMessageIdLong() == messageConfigManager.getMessage()) {

            if (!event.getUser().isBot() && !event.getUser().getName().equals("ColorBot")) {

                String rank = event.getReactionEmote().getName().substring(5);

                if (CollectionUtils.containsIgnoreCase(rankConfigManager.getRanks().keySet(), rank)) {

                    event.getGuild().addRoleToMember(event.getMember(), event.getGuild().getRolesByName(rank, true).get(0)).queue();
                    System.out.println(String.format("Added %s role to %d.", rank, event.getUserIdLong()));

                }

            }

        }

    }

    @Override
    public void onGuildMessageReactionRemove(@Nonnull GuildMessageReactionRemoveEvent event) {

        if (event.getMessageIdLong() == messageConfigManager.getMessage()) {

            String rank = event.getReactionEmote().getName().substring(5);

            if (CollectionUtils.containsIgnoreCase(rankConfigManager.getRanks().keySet(), rank)) {

                event.getGuild().removeRoleFromMember(event.getUserIdLong(), event.getGuild().getRolesByName(rank, true).get(0)).queue();
                System.out.println(String.format("Removed %s role from %d.", rank, event.getUserIdLong()));

            }

        }

    }

}
