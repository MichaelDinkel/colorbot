package com.kaixeleron.colorbot.bot;

import com.kaixeleron.colorbot.config.MessageConfigManager;
import com.kaixeleron.colorbot.config.RankConfigManager;
import com.kaixeleron.colorbot.util.CollectionUtils;
import net.dv8tion.jda.api.entities.ListedEmote;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.util.List;

public class MessageListener extends ListenerAdapter {

    private final MessageConfigManager configManager;
    private final RankConfigManager rankConfigManager;

    public MessageListener(MessageConfigManager configManager, RankConfigManager rankConfigManager) {

        this.configManager = configManager;
        this.rankConfigManager = rankConfigManager;

    }

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {

        if (!configManager.isMessageSet()) {

            if (event.getAuthor().isBot() && event.getAuthor().getName().equals("ColorBot")) {

                configManager.setMessage(event.getMessageIdLong());

                List<ListedEmote> emotes = event.getGuild().retrieveEmotes().complete();

                for (ListedEmote emote : emotes) {

                    if (CollectionUtils.containsIgnoreCase(rankConfigManager.getRanks().keySet(), emote.getName().substring(5))) {

                        event.getMessage().addReaction(emote).queue();

                    }

                    /*boolean found = false;
                    int i = 0;

                    while (!found && i < rankEmotes.length) {

                        if (rankEmotes[i].equalsIgnoreCase(emote.getName().substring(5))) {

                            event.getMessage().addReaction(emote).queue();
                            found = true;

                        }

                        i++;

                    }*/

                }

            }

        }

    }

}
