package com.kaixeleron.colorbot.bot;

import com.kaixeleron.colorbot.config.MessageConfigManager;
import com.kaixeleron.colorbot.config.RankConfigManager;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.imageio.ImageIO;
import javax.security.auth.login.LoginException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BotThread extends Thread {

    private RankConfigManager rankConfigManager;
    private MessageConfigManager messageConfigManager;

    private final String key, channel;
    private JDA jda;

    private volatile boolean success = false;

    public BotThread(String key, String channel) {

        this.key = key;
        this.channel = channel;

    }

    @Override
    public void run() {

        rankConfigManager = new RankConfigManager();
        messageConfigManager = new MessageConfigManager();

        try {

            try {

                jda = JDABuilder.createLight(key, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS).disableCache(CacheFlag.EMOTE).addEventListeners(new MessageListener(messageConfigManager, rankConfigManager), new ReactionListener(messageConfigManager, rankConfigManager)).build().awaitReady();

            } catch (InterruptedException ignored) {}

            success = true;

            synchronized (this) {

                notifyAll();

            }

            createRanks();
            createEmotes();
            announce();

        } catch (LoginException e) {

            System.err.println("Could not log into Discord bot account.");
            e.printStackTrace();

        }

    }

    public boolean isConnectionSuccessful() {

        return success;

    }

    public void disconnect() {

        jda.shutdown();

    }

    private void createRanks() {

        Map<String, Color> ranks = rankConfigManager.getRanks();

        for (Guild guild : jda.getGuilds()) {

            List<String> roles = new ArrayList<>();

            for (Role role : guild.getRoles()) {

                roles.add(role.getName());

            }

            int numCreated = 0;

            for (String rank : ranks.keySet()) {

                if (!roles.contains(rank)) {

                    System.out.println(String.format("Creating rank %s...", rank));

                    guild.createRole().setName(rank).setColor(ranks.get(rank)).complete();
                    numCreated++;

                }

            }

            if (numCreated > 0) {

                System.out.println(String.format("Created %d roles on guild %d.", numCreated, guild.getIdLong()));

            }

        }

    }

    private void createEmotes() {

        Map<String, Color> ranks = rankConfigManager.getRanks();

        for (Guild guild : jda.getGuilds()) {

            List<String> emotes = new ArrayList<>();

            for (Emote emote : guild.retrieveEmotes().complete()) {

                emotes.add(emote.getName());

            }

            int numCreated = 0;

            for (String rank : ranks.keySet()) {

                if (!emotes.contains(String.format("rank_%s", rank.toLowerCase()))) {

                    System.out.println(String.format("Creating emote %s...", rank));

                    BufferedImage image = new BufferedImage(128, 128, BufferedImage.TYPE_3BYTE_BGR);
                    Graphics2D graphics = image.createGraphics();
                    graphics.setColor(ranks.get(rank));
                    graphics.fillRect(0, 0, 128, 128);
                    graphics.dispose();

                    ByteArrayOutputStream os = new ByteArrayOutputStream();

                    try {

                        ImageIO.write(image, "png", os);

                    } catch (IOException ignored) {}

                    guild.createEmote(String.format("rank_%s", rank.toLowerCase()), Icon.from(os.toByteArray(), Icon.IconType.PNG)).complete();
                    numCreated++;

                    try {

                        os.close();

                    } catch (Exception ignored) {}

                }

            }

            if (numCreated > 0) {

                System.out.println(String.format("Created %d emotes on guild %d.", numCreated, guild.getIdLong()));

            }

        }

    }

    private void announce() {

        if (!messageConfigManager.isMessageSet()) {

            for (Guild guild : jda.getGuilds()) {

                for (TextChannel channel : guild.getTextChannelsByName(channel, false)) {

                    Message message = new MessageBuilder("React to this message with the color ranks you want.").build();

                    channel.sendMessage(message).queue();

                }

            }

        }

    }

}
