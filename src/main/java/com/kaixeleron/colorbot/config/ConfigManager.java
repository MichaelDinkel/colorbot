package com.kaixeleron.colorbot.config;

import java.io.*;

public class ConfigManager {

    private boolean keyMissing, channelMissing;

    private String key, channel;

    public ConfigManager() {

        File configFolder = new File("ColorBotConfig");
        //noinspection ResultOfMethodCallIgnored
        configFolder.mkdir();

        File configFile = new File(configFolder, "config.txt");

        BufferedWriter writer = null;

        try {

            keyMissing = configFile.createNewFile();
            channelMissing = keyMissing;

            if (keyMissing) {

                writer = new BufferedWriter(new FileWriter(configFile));
                writer.write("key=\nchannel=");
                writer.flush();

            }

        } catch (IOException e) {

            System.err.println("Could not create config.txt file.");
            keyMissing = true;

        } finally {

            if (writer != null) {

                try {

                    writer.close();

                } catch (Exception ignored) {}

            }

        }

        if (!keyMissing) {

            try (BufferedReader reader = new BufferedReader(new FileReader(configFile))) {

                String line = reader.readLine();

                while (line != null) {

                    if (line.contains("=")) {

                        String[] split = line.split("=");

                        if (split[0].equals("key")) {

                            if (split.length > 1) {

                                key = split[1];
                                if (key.length() > 0) keyMissing = false;

                            } else {

                                keyMissing = true;

                            }

                        } else if (split[0].equals("channel")) {

                            if (split.length > 1) {

                                channel = split[1];
                                channelMissing = false;

                            } else {

                                channelMissing = true;

                            }

                        }

                    }

                    line = reader.readLine();

                }

            } catch (IOException e) {

                key = "";
                channel = "";
                System.err.println("Could not read config.txt file.");
                e.printStackTrace();

            }

            if (key == null || key.length() == 0) {

                keyMissing = true;

            }

        }

    }

    public String getKey() {

        return key;

    }

    public String getChannel() {

        return channel;

    }

    public boolean isKeyMissing() {

        return keyMissing;

    }

    public boolean isChannelMissing() {

        return channelMissing;

    }

}
