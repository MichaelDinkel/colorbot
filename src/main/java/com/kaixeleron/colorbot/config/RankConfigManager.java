package com.kaixeleron.colorbot.config;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class RankConfigManager {

    private final Map<String, Color> ranks;

    public RankConfigManager() {

        ranks = new HashMap<>();

        File configFolder = new File("ColorBotConfig");
        //noinspection ResultOfMethodCallIgnored
        configFolder.mkdir();

        File rankConfigFile = new File(configFolder, "ranks.txt");

        if (!rankConfigFile.exists()) {

            try {

                Files.copy(getClass().getResourceAsStream("/ranks.txt"), rankConfigFile.toPath());

            } catch (IOException e) {

                System.err.println("Could not create ranks.txt file.");
                e.printStackTrace();

            }

        }

        try (BufferedReader reader = new BufferedReader(new FileReader(rankConfigFile))) {

            String line = reader.readLine();

            while (line != null) {

                if (line.contains("=")) {

                    String[] split = line.split("=");

                    ranks.put(split[0], Color.decode(split[1]));

                }

                line = reader.readLine();

            }

        } catch (IOException e) {

            System.err.println("Could not read ranks.txt file.");
            e.printStackTrace();

        }

    }

    public Map<String, Color> getRanks() {

        return ranks;

    }

}
