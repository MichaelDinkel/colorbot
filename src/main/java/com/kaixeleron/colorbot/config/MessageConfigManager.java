package com.kaixeleron.colorbot.config;

import java.io.*;

public class MessageConfigManager {

    private final File configFile;

    private long message = -1L;

    public MessageConfigManager() {

        File configFolder = new File("ColorBotConfig");
        //noinspection ResultOfMethodCallIgnored
        configFolder.mkdir();

        configFile = new File(configFolder, "message.txt");

        try {

            //noinspection ResultOfMethodCallIgnored
            configFile.createNewFile();

        } catch (IOException e) {

            System.err.println("Could not create message.txt file.");
            e.printStackTrace();

        }

        try (BufferedReader reader = new BufferedReader(new FileReader(configFile))) {

            String line = reader.readLine();

            if (line != null) {

                try {

                    message = Long.parseLong(line);

                } catch (NumberFormatException e) {

                    System.err.println("Invalid message ID in message.txt.");

                }

            }

        } catch (IOException e) {

            System.err.println("Could not read message.txt file.");
            e.printStackTrace();

        }

    }

    public long getMessage() {

        return message;

    }

    public void setMessage(long message) {

        this.message = message;

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(configFile))) {

            writer.write(Long.toString(message));
            writer.flush();

        } catch (IOException e) {

            System.err.println("Could not write to message.txt file.");
            e.printStackTrace();

        }

    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isMessageSet() {

        return message != -1L;

    }

}
